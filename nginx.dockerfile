FROM nginx:alpine
LABEL author="Frederick John"
COPY ./config/nginx.conf /etc/nginx/conf.d/default.conf
# COPY /dist/stockmaster /usr/share/nginx/html

# Use the following commands to build the image and run the container (run from the root folder)
# 1. You'll first need to build the project using `ng build`

# 2. Now build the Docker image:
# docker build -t nginx-cont -f nginx.dockerfile .

#3. Run the Docker container:
# To run the container we'll create a volume to point to our local source code.

# docker run -p 8080:80 -v $(pwd)/dist/stockmaster:/usr/share/nginx/html nginx-cont
