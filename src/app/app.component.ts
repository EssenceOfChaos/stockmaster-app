import { Component } from '@angular/core';
import { environment } from '../environments/environment';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'StockMaster';
  production = environment.production;
  constructor() {
    console.log(`Currently in production: ${this.production}`);
  }
}
