// Environment Configuration
require('dotenv').config()
// Imports
const express = require('express');
const path = require('path');
const mongoose = require('mongoose');

const bodyParser = require('body-parser');
const database = require('./server/config/db');
const logger = require('./server/config/logger');
const pkg = require('./package.json');
// const userRoutes = require('./routes/user.route');
const portfolioRoutes = require('./server/routes/portfolio.route');

let mongo_uri;

if (process.env.NODE_ENV !== 'production') {
  logger.info("Node is starting in DEVELOPMENT mode");
}

if (process.env.NODE_ENV == 'production') {
  mongo_uri = process.env.MONGODB_PROD
} else {
  mongo_uri = process.env.MONGODB_DEV
}

console.log(mongo_uri);

// Connecting mongoDB
mongoose.Promise = global.Promise;
mongoose.connect(database.mongodb, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(() => {
  console.log('Database connected sucessfully ')
},
  error => {
    console.log('Could not connected to database : ' + error)
  }
)

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//CORS
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-XSRF-TOKEN, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
  next();
});

// Middleware
app.use(function (req, res, next) {
  // let ts = Date.now();
  // console.log(`Request received at ${ts}`);
  logger.info(`${req.method} - ${req.url}`);
  next()
})

app.get('/version', (req, res) => {
  res.send(pkg.version);
});

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log('ERROR: ' + reason);
  res.status(code || 500).json({ error: message });
}

// Create link to Angular build directory
let distDir = __dirname + "/dist/stockmaster";
app.use(express.static(distDir));

app.use('/portfolios', portfolioRoutes);

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/dist/stockmaster/index.html'));
});

// Find 404 and hand over to error handler
app.use((req, res, next) => {
  var err = new Error('Not Found');
  logger.error(`${req.url} not available!`)
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  console.error(err.message);
  if (!err.statusCode) err.statusCode = 500;
  res.status(err.statusCode).send(err.message);
});

// Assign port
const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
  console.log('Connected to port ' + port)
})

module.exports = server;
