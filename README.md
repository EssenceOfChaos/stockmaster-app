# Stockmaster

## Getting Started

Use the following commands to build the image and run the container (run from the root folder)

### 1. You'll first need to build the project using

`ng build --watch --delete-output-path false`

### 2. Now build the Docker image

`docker build -t nginx-cont -f nginx.dockerfile .`

### 3. Run the Docker container

To run the container we'll create a volume to point to our local source code.

`docker run -p 8080:80 -v $(pwd)/dist/stockmaster:/usr/share/nginx/html nginx-cont`

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.0.

## Production server

`NODE_ENV=production npm start`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Publish to Heroku

`git push heroku master`
