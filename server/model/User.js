const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let User = new Schema(
  {
    name: {
      type: String
    },
    email: {
      type: String
    },
    picture: {
      type: String
    },
    email_verified: {
      type: Boolean
    },
    sub: {
      type: String
    },
    has_portfolio: {
      type: Boolean
    }
  },
  { collection: 'users' }
);

module.exports = mongoose.model('User', User);
